------ IBOX -----


Instructions for installing and building on Android
	- Clone master branch of repo
	- Launch Android Studio and open the iBox/Android_Build folder
	- Set your Android device into developer mode
	- Enable USB debugging on your device
	- Plug in USB to PC and Android device
	- Select your device in the upper right of Android Studio between "app" and green play button
	- For debugging mode, select the green bug to the right of the green play button
	- For simple run mode, select the green play button.
	- The app will build and install on your device


Instructions for installing and building on IOS
	- Clone master branch of repo
	- Launch Xcode and open iBox/IOS_Build folder
	- Go to Xcode>preferences>accounts>plus sign on bottom left> add Apple ID account
	- Sign in using Apple ID and Password
	- Connect device to Mac and allow access to pop up message
	- Under Folder icon>Signing & Capabilities>Team> add in the apple account
	- Wait till Xcode finished confirming device while going to
	  settings>general>device management>click account name>enable trust
	- Confirm the device selected in Xcode is your device
	- Press play button in Xcode and the app will build and run your device