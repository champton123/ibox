			---- Android Productivity Metric ----
	- 1037 Lines produced / 30 hours = 35 LOC/hour


			---- Android Q.O.C Metric ----
	- 0 Faults present in current build / 1.3K Lines of Code



			---- IOS Productivity Metric ----
	- 772 Lines produced / 25 hours = 31 LOC/hour


			---- IOS Q.O.C Metric
	- 5 Faults present in current build / 1k Lines of Code
		-  Thread 1 signal: SIGTERM is a default error whenever the default simulator and its properties changed. It does not affect the build