package com.example.iboxandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;

public class MediaUpload extends AppCompatActivity {

    public static final int GALLERY_INTENT = 2;
    private StorageReference storage;
    private ProgressDialog progressSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_upload);

        storage = FirebaseStorage.getInstance().getReference();
        progressSpinner = new ProgressDialog(this);

        configureBackButton();
        configureBrowseFilesButton();
    }

    private void configureBackButton(){
        ImageButton backButton = (ImageButton) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void configureBrowseFilesButton()
    {
        ImageButton browseFilesButton = (ImageButton) findViewById(R.id.browseButton);

        browseFilesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, GALLERY_INTENT);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == GALLERY_INTENT && resultCode == RESULT_OK)
        {
            progressSpinner.setMessage("Uploading...");
            progressSpinner.show();

            final Uri uri = data.getData();

            final StorageReference filepath = storage.child("Photos").child("image" + uri.getLastPathSegment());

            filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Toast.makeText(MediaUpload.this, "Upload Done.", Toast.LENGTH_LONG).show();
                    progressSpinner.dismiss();

                    filepath.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            DatabaseReference imageStore = FirebaseDatabase.getInstance().getReference().child("Image");

                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("imageUrl", String.valueOf(uri));

                            imageStore.setValue(hashMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText( MediaUpload.this, "Finally Completed", Toast.LENGTH_SHORT);
                                }
                            });
                        }
                    });

                }
            });
        }
    }
}

