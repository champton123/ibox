//
//  SecondViewController.swift
//  AdminUserInterface3
//
//  Created by Alex Correia on 5/18/20.
//  Copyright © 2020 Alex Correia. All rights reserved.
//

import UIKit
import FileProvider

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func VideoButton(_ sender: Any) {
        //Show text saying upload media
        present(VideosViewController(), animated: true)
        print("Video Uploaded")
    }
    
    @IBAction func CommentButton(_ sender: Any) {
        present(CommentsViewController(), animated: true)
        print("Comment wrote and sent")
    }
    
    @IBAction func ImageButton(_ sender: Any) {
        present(ImagesViewController(), animated: true)
        print("Image Clicked")
    }
    

    @IBAction func AudioButton(_ sender: Any) {
        print("Audio Uploaded")
    }
    
    @IBAction func BrowseFButton(_ sender: Any) {
       // present(UIDocumentBrowserViewController(), animated: true)
          
        
        print("Open window to browse files")
    }
}
class VideosViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        
        

        
    }
        
    

}

class CommentsViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var ScrollView: UIScrollView!
    @IBOutlet weak var TextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white;
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    

}

class ImagesViewController: UIViewController {

override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white;
    }
    
//code crashes need to modify
/*
func uploadImage(){
    print("you have accessed and uploaded from your gallery")
    var imageToUpload:UIImage = UIImage()

    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
    if let dirPath          = paths.first
    {
        let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("") //Your image name here
        let image    = UIImage(contentsOfFile: imageURL.path)
        imageToUpload = image!
        
    }
    
  }

*/
}

class UIDocumentBrowserViewController : UIViewController{
    
    override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white;
    }
    
}

