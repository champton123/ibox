//
//  ViewController.swift
//  AdminUserInterface3
//
//  Created by Alex Correia on 4/30/20.
//  Copyright © 2020 Alex Correia. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var vRoundButton1: UIButton!
    
    @IBOutlet weak var vRoundButton2: UIButton!
    
    @IBOutlet weak var vRoundButton3: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.vRoundButton1.layer.cornerRadius = 20
        self.vRoundButton2.layer.cornerRadius = 20
        self.vRoundButton3.layer.cornerRadius = 20
        // Do any additional setup after loading the view.
    }


}

