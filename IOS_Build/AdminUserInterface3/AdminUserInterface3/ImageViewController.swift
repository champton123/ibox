//
//  ImageViewController.swift
//  AdminUserInterface3
//
//  Created by Alex Correia on 5/21/20.
//  Copyright © 2020 Alex Correia. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func uploadFunction(_ sender: Any) {
          var imageToUpload:UIImage = UIImage()

          let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
          let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
          let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
          if let dirPath          = paths.first
          {
              let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("") //Your image name here
              let image    = UIImage(contentsOfFile: imageURL.path)
              imageToUpload = image!
              
          }
            
            print("Image uploaded")
          
        }
    }

