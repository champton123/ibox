//
//  ThirdViewController.swift
//  userInterface
//
//  Created by Anthony Stafford on 5/23/20.
//  Copyright © 2020 ibox. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBAction func uploadImage(_ sender: Any) {
        
        let testImage = UIImagePickerController()
        testImage.delegate = self
        
        testImage.sourceType = UIImagePickerController.SourceType.photoLibrary
        testImage.allowsEditing = false
        self.present(testImage, animated: true)
        
        //upload image to server code here
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "background")?.draw(in: self.view.bounds)
        let bg: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: bg)

        // Do any additional setup after loading the view.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
