//
//  HomeViewController.swift
//  CostumLoginDemo
//
//  Created by ZHENG ZHENG on 5/26/20.
//  Copyright © 2020 ZHENG ZHENG. All rights reserved.
//

import UIKit
import SwiftUI

class HomeViewController: UIViewController {

    @IBOutlet weak var adminenter: UIButton!
    @IBOutlet weak var userenter: UIButton!
    @IBOutlet weak var logout: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBSegueAction func swiftUIactoin(_ coder: NSCoder) -> UIViewController? {
        return UIHostingController(coder: coder, rootView: ContentView())
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func setUpElements(){
        Utilities.styleFilledButton(adminenter)
        Utilities.styleFilledButton(userenter)
        Utilities.styleFilledButton(logout)
    }
    @IBAction func logouttap(_ sender: Any) {
       let homeViewController2 = storyboard?.instantiateViewController(identifier: Constants2.Storyboard2.homeViewController2) as? ViewController
        
        view.window?.rootViewController = homeViewController2
        view.window?.makeKeyAndVisible()
    }
    

}

struct HomeViewController_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
