//
//  NewPostViewController.swift
//  FirebasePhotos
//
//  Created by Duc Tran on 10/9/17.
//  Copyright © 2017 Duc Tran. All rights reserved.
//

import UIKit

class NewPostViewController: UIViewController
{
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var captionTextView: UITextView!
    var textViewPlaceholderText = "What's on your mind?"
    
    var takenImage: UIImage!
    var imagePicker: UIImagePickerController!
    var didShowCamera = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !didShowCamera {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                imagePicker.cameraCaptureMode = .photo
            } else {
                imagePicker.sourceType = .photoLibrary
            }
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        captionTextView.text = textViewPlaceholderText
        captionTextView.textColor = .lightGray
        captionTextView.delegate = self
        
        
    }
    
    @IBAction func shareDidTap()
    {
        if captionTextView.text != textViewPlaceholderText && captionTextView.text != "" && takenImage != nil {
            let newPost = Post(image: self.takenImage, caption: self.captionTextView.text)
            newPost.save()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelDidTap(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
}

extension NewPostViewController : UITextViewDelegate
{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == textViewPlaceholderText {
            textView.text = ""
            textView.textColor = .white
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = textViewPlaceholderText
            textView.textColor = .lightGray
        }
        textView.resignFirstResponder()
    }
}

extension NewPostViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as! UIImage
        print(image)
        self.postImageView.image = takenImage
        self.takenImage = image
        didShowCamera = true
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}












