//
//  PhotoTableViewCell.swift
//  FirebasePhotos
//
//  Created by Duc Tran on 10/9/17.
//  Copyright © 2017 Duc Tran. All rights reserved.
//

import UIKit
import Firebase

class PhotoTableViewCell: UITableViewCell {

    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var captionLabel: UILabel!
    @IBOutlet weak var shadowBackgroundView: UIView!
    
    var post: Post! {
        didSet {
            self.updateUI()
        }
    }
    
    func updateUI() {
        // Set shadow background view
        shadowBackgroundView.layer.shadowPath = UIBezierPath(rect: shadowBackgroundView.bounds).cgPath
        shadowBackgroundView.layer.shadowColor = UIColor.black.cgColor
        shadowBackgroundView.layer.shadowOpacity = 0.1
        shadowBackgroundView.layer.shadowOffset = CGSize(width: 2, height: 2)
        shadowBackgroundView.layer.shadowRadius = 2
        shadowBackgroundView.layer.masksToBounds = false
        shadowBackgroundView.layer.cornerRadius = 3.0
        
        // caption
        self.captionLabel.text = post.caption
        
        // download image
        if let imageDownloadURL = post.downloadURL {
            let imageStorageRef = Storage.storage().reference(forURL: imageDownloadURL)
            imageStorageRef.getData(maxSize: 2 * 1024 * 1024) { [weak self] (data, error) in
                if let error = error {
                    print("******** \(error)")
                } else {
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        DispatchQueue.main.async {
                            self?.postImageView.image = image
                        }
                    }
                }
                
            }
        }
    }

}
















